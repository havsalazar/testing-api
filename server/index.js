/* eslint-disable camelcase */
'use strict';

var testServer = require('./server.js');
const testData = require('./../data/BillingData');

function TestApi() {
  TestApi.prototype.NewNotificationFromBilling = function test(params) {
    const data = params | testData.BillingNewNotificationData;
    const Producer = testServer.models.producer;
    Producer.newNotification(data);
  };
  TestApi.prototype.disable2fa = function test(params) {
    const Producer = testServer.models.producer;
    Producer.disable2fa(params);
  };
}
module.exports = TestApi;

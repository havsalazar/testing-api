'use strict';

require('./common');

describe('Initialization', function() {
  let app = require('./../server/server.js');

  before(function(done) {
    // app = requireUncached(TEST_APP);
    app.start();
    app.once('started', done);
  });

  describe('Component initialized', function() {
    it('Mixins Running', function() {
      expect(app.models.producer.newNotification).to.be.a('function');
    });
  });
});

// 'use strict';
// var testServer = require('./../server/server.js');
// const testData = require('./../data/BillingData');
// const data = testData.BillingNewNotificationData;
// const Producer = testServer.models.producer;
// console.log(Producer.newNotification(data));

'use strict';

require('./common');
const testData = require('./../data/BillingData');
const data = testData.BillingNewNotificationData;

describe('Initialization', function() {
  let app = require('./../server/server.js');

  before(function(done) {
    // app = requireUncached(TEST_APP);
    app.start();
    app.once('started', done);
  });

  describe('Component initialized', function() {
    it('Send message', function() {
      const Producer = app.models.producer;
      expect(Producer.newNotification(data)).to.be.a('promise');
    });
  });
});

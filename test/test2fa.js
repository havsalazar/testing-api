/* eslint-disable max-len */
'use strict';
const app = require('./../server/server.js');
const token = 'eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhhdnNhbGF6YXJAZ21haWwuY29tIiwibGFuZ3VhZ2UiOiJlcyIsImlzcyI6IjVkMzBlYzY2ZDViOTVmMDBkMjk0ZDU1NSIsInVzciI6IjVkM2YzODVhMzRiZTUzMDZhN2QwMDVjZSIsImp0aSI6ImowMWhSbVBoM2lSa0tQRU5sZjVmUWRpWXlzTDE3d3JRZTlhR29qT0EwODRTMWhud0dwMEh5UEszbUQzS09rUVgiLCJhdWQiOiJ0cmFuc2FjdGlvbixpZGVudGl0eSxhdXRoIiwibWV0YWRhdGEiOiJ7fSIsImlhdCI6MTU3NjUzOTEzNCwiZXhwIjoxNTc2NTQ5OTM0fQ.r1nPR63KfJhB0PW5mJe6lNQqAOf6OLp2JQTq_tEEoMmVRimSWRZG6h8yTZJbuPmur1VPk4Yr4DLyZ9A92Y3V9g';
app.start();
app.once('started', ()=>{
  const Producer = app.models.producer;
  Producer.disable2fa({
    clientId: '5d30ec66d5b95f00d294d555',
    token,
  }).then((response)=>{
    console.log(response.body);
  }).catch(err=>console.log(err));

  Producer.get2faCode({
    clientId: '5d30ec66d5b95f00d294d555',
    token,
    format: 'uri',
  }).then((response)=>{
    console.log(response.body);

    ask('   > Enter the authenticator code', function(code) {
      Producer.active2fa({
        clientId: '5d30ec66d5b95f00d294d555',
        token,
        totp: code,
      }).then((response)=>{
        console.log(response.body);
      }).catch(err=>console.log(err));
    });
  }).catch(err=>console.log(err));
});

function ask(question, callback) {
  var stdin = process.stdin;
  var stdout = process.stdout;

  stdin.resume();
  stdout.write(question + ': ');

  stdin.once('data', function(data) {
    data = data.toString().trim();
    callback(data);
  });
}

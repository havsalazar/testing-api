/* eslint-disable camelcase */
'use strict';
module.exports = {
  BillingNewNotificationData: {
    userId: '',
    namespace: 'billing:invouce:new-invoice',
    notification_data: JSON.stringify({
      date: new Date(),
      products: [],
      subtotal: 1,
      iva: 1,
      total: 1,
    }),
  },

};

# Testing Api For coinsenda micro-services
 

### How Use


```
    const testApi = require('testing-api'); // require library

    const testApp = new testApi(); #instance 

    testApp.NewNotificationFromBilling(); # execute method

    // OR

    testApp.NewNotificationFromBilling({
        userId: '',
        namespace: 'billing:invouce:new-invoice',
        notification_data: JSON.stringify({
            date: new Date(),
            products: [],
            subtotal: 1,
            iva: 1,
            total: 1,
        }),
    };
 

```



### Available methods

 - NewNotificationFromBilling ( object "optional param" );



### Default data path

All default data it's save on data root folder 


| Service | File name |
| ------- | --------- |
| Billing | BillingData.js |
